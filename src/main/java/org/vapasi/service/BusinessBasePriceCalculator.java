package org.vapasi.service;

import org.vapasi.model.FlightRoute;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

public class BusinessBasePriceCalculator  extends Calculator{
    public BusinessBasePriceCalculator() throws ParseException {
    }

    Date dateObj = new Date();
    Date bookingOpenDateForBusiness = addDays(dateObj, -28);


    @Override
    public double calculateBasePriceValue(FlightRoute flightRoute, int noOfPassengers, String classOfTravel,double basePrice) {
        Date date = flightRoute.getDepartureDate();
        if (date.after(bookingOpenDateForBusiness)) {
            Calendar cal = Calendar.getInstance();

            cal.setTime(date);
            String dateString = cal.toString();
            // System.out.println();
            int day = cal.get(Calendar.DAY_OF_WEEK);
            //System.out.println(day);

            if (day == 2 || day == 6 || day == 1) {
                basePrice = 1.4 * basePrice;
            }
        } else {
            basePrice = basePrice;
        }
        return basePrice;
    }



}

