package org.vapasi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.vapasi.model.FlightRoute;
import org.vapasi.model.TravelClass;
import org.vapasi.repository.FlightRepository;
import java.text.ParseException;
import java.util.*;
import java.util.stream.Collectors;


@Service
public class SearchService {
    public SearchService() throws ParseException {

    }

    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }


    FlightRepository rep;
    @Autowired
    public void setRep(FlightRepository rep) {
        this.rep = rep;
    }

    public static String getDateInFormat(Date someDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(someDate);
        String dateInString = cal.get(Calendar.DATE) + "/" +
                (cal.get(Calendar.MONTH) + 1) +
                "/" + cal.get(Calendar.YEAR);
        return dateInString;
    }
List<FlightRoute> newFlight = new ArrayList<>();
    Date dateObj = new Date();
    Calculator calculator = new EconomyBasePriceCalculator();
    Calculator calculator1 = new BusinessBasePriceCalculator();
    Calculator calculator2 = new FirstClassBasePriceCalculator();





    public List<FlightRoute> searchFlights(String source, String destination, int noOfPassengers, Date departureDate, String classOfTravel) throws ParseException {

       /* try {
            rep = new FlightRepository();
        } catch (ParseException e) {
            e.printStackTrace();
        }*/
        List<TravelClass> travelClassList = rep.retrieveTravelClasses();
        List<FlightRoute> flights = rep.retrieveSourceDestinationMapping();
        List<FlightRoute> newFlight = new ArrayList();
        String formattedDate = getDateInFormat(departureDate);
        // String formattedDate1 = getDateInFormat(flight.getDepartureDate());
        newFlight = flights.stream().filter(flight -> ((source.equals(flight.getSource())) && (destination.equals(flight.getDestination())) && noOfPassengers >= 1 && departureDate.after(dateObj) && formattedDate.equals(getDateInFormat(flight.getDepartureDate())))).collect(Collectors.toList());
        for (FlightRoute f : newFlight) {
            for(TravelClass t:travelClassList){
                if(t.getClassOfTravel().equals(classOfTravel)&&t.getRouteID()==f.getRouteID()){
                    double basePrice = t.getBasePrice();
                    if(classOfTravel.equals("economy")){
                        basePrice = calculator.calculateBasePriceValue(f,noOfPassengers,classOfTravel,t.getBasePrice());
                    }
                    else if(classOfTravel.equals("Business")){
                        basePrice = calculator1.calculateBasePriceValue(f,noOfPassengers,classOfTravel,t.getBasePrice());
                    }
                    else if(classOfTravel.equals("FirstClass")){
                        basePrice = calculator2.calculateBasePriceValue(f,noOfPassengers,classOfTravel,t.getBasePrice());
                    }
                    else
                    {
                        basePrice = 0;
                    }
                    //t.setBasePrice(basePrice);
                    f.setTotalFare(calculator.calculateTotalFare(noOfPassengers,basePrice));
                }
            }
            if (f.getTotalFare() == 0) {
                newFlight.remove(f);
            }
        }
        return newFlight;
    }

  }


