package org.vapasi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.vapasi.model.FlightRoute;
import org.vapasi.model.TravelClass;
import org.vapasi.repository.FlightRepository;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public abstract class Calculator {

    FlightRepository rep = new FlightRepository();
    private static int noOfSeatsFilled;
    private static int seats;

    public Calculator() throws ParseException {

    }


    public static Date addDays(Date date, int days) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, days); //minus number would decrement the days
        return cal.getTime();
    }


@Autowired
    TravelClass travelClass;

    public abstract double calculateBasePriceValue(FlightRoute flightRoute, int noOfPassengers, String classOfTravel,double basePrice);

    public double calculateTotalFare(int noOfPassengers,double basePriceValue) {

        double totalFare = 0;
        totalFare = totalFare +(noOfPassengers*basePriceValue);
        System.out.println(totalFare);
        return  totalFare;

    }

}
