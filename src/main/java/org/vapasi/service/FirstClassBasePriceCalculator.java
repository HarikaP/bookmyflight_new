package org.vapasi.service;

import org.vapasi.model.FlightRoute;

import java.text.ParseException;
import java.util.Date;

public class FirstClassBasePriceCalculator extends Calculator {
    public FirstClassBasePriceCalculator() throws ParseException {
    }

    @Override
    public double calculateBasePriceValue(FlightRoute flightRoute, int noOfPassengers, String classOfTravel, double basePrice) {
        Date bookingDate = new Date();
        long diff = (flightRoute.getDepartureDate().getTime() - bookingDate.getTime()) / (1000 * 60 * 60 * 24);

        for (double n = 0; n <= 10; n++) {
            if (diff == n) {
                double x = 1 + ((10 - n) / 10);
                // System.out.println(x);
                basePrice = x * basePrice;
            }
        }
        return basePrice;
    }


}
