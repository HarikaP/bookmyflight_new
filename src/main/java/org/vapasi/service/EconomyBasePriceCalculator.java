package org.vapasi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.vapasi.model.FlightDetails;
import org.vapasi.model.FlightRoute;
import org.vapasi.model.TravelClass;
import org.vapasi.repository.FlightRepository;

import java.text.ParseException;
import java.util.List;

public class EconomyBasePriceCalculator extends Calculator{
    private static int noOfSeatsFilled;
    private static int seats;
    private int totalSeats;
    public EconomyBasePriceCalculator() throws ParseException {


    }


    FlightRepository rep = new FlightRepository();


    public void setRep(FlightRepository rep) {
        this.rep = rep;
    }


    List<TravelClass> travelClassList = rep.retrieveTravelClasses();
    List<FlightDetails> flightDetailsList= rep.retrieveFlightsList();;


    @Override
    public double calculateBasePriceValue(FlightRoute flightRoute, int noOfPassengers, String classOfTravel,double basePriceValue) {

        for(FlightDetails f:flightDetailsList) {
            if (flightRoute.getFlightID() == f.getFlightID()) {
                for (TravelClass t : travelClassList) {

                    if (t.getRouteID() == flightRoute.getRouteID() &&t.getClassOfTravel().equals(classOfTravel)) {
                        seats = t.getNoOfRemainingPassengers();
                        totalSeats = f.getNoOfEconomySeats();
                        noOfSeatsFilled = noOfPassengers + (totalSeats - seats);
                        seats = totalSeats - noOfSeatsFilled;
                        if (seats > (0.6 * totalSeats)) {
                            basePriceValue = t.getBasePrice();
                        }
                        else if ((seats > (0.1 *totalSeats )) && (seats <= (0.6 * totalSeats))) {
                            basePriceValue = 1.3 * t.getBasePrice();
                        } else {
                            basePriceValue = 1.6 * t.getBasePrice();
                        }
                    }
                }

            }
        }

        return basePriceValue;
    }
}
