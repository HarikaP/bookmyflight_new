package org.vapasi.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.vapasi.model.FlightDetails;
import org.springframework.stereotype.Repository;
import org.vapasi.model.FlightRoute;
import org.vapasi.model.TravelClass;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

@Repository
public class FlightRepository {
    @Autowired
    JdbcTemplate jdbcTemplate;

    private List<String> flight,city;
    private List<FlightRoute> sourceDestinationFlights;
    private List<FlightDetails> flightsList;
    List<TravelClass> travelClassList = new ArrayList<>();


    String sDate1="31/08/2019";
    String sDate2="30/08/2019";
    String sDate3="30/08/2019";
    String sDate4="03/09/2019";
    String sDate5="25/08/2019";
    String sDate6="29/08/2019";
    String sDate7="25/08/2019";
    String sDate8="26/08/2019";



    Date date1=new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
    Date date2=new SimpleDateFormat("dd/MM/yyyy").parse(sDate2);
    Date date3=new SimpleDateFormat("dd/MM/yyyy").parse(sDate3);
    Date date4=new SimpleDateFormat("dd/MM/yyyy").parse(sDate4);
    Date date5=new SimpleDateFormat("dd/MM/yyyy").parse(sDate5);
    Date date6=new SimpleDateFormat("dd/MM/yyyy").parse(sDate6);
    Date date7=new SimpleDateFormat("dd/MM/yyyy").parse(sDate7);
    Date date8=new SimpleDateFormat("dd/MM/yyyy").parse(sDate8);


    FlightDetails flight1 = new FlightDetails(1001,"Boeing 777-200LR(77L)",195,35,8);
    FlightDetails flight2 = new FlightDetails(1002,"Airbus A319 V2",144,0,0);
    FlightDetails flight3 = new FlightDetails(1003,"Airbus A321",152,20,0);

    FlightRoute route1 = new FlightRoute(1,1001,"Hyderabad","Delhi",date1);
    FlightRoute route2 = new FlightRoute(2,1002,"Chennai","Bangalore",date2);
    FlightRoute route3 = new FlightRoute(3,1003,"Delhi","Bangalore",date3);
    FlightRoute route4 = new FlightRoute(4,1001,"Hyderabad","Pune",date4);
    FlightRoute route5 = new FlightRoute(5,1002,"Chennai","Delhi",date5);
    FlightRoute route6 = new FlightRoute(6,1002,"Delhi","Bangalore",date6);
    FlightRoute route7 = new FlightRoute(7,1002,"Pune","Bangalore",date7);
    FlightRoute route8 = new FlightRoute(8,1003,"Delhi","Chennai",date8);



    public FlightRepository() throws ParseException {
        this.flight = new ArrayList<String>();
        this.city = new ArrayList<String>();
        addFlightDetails();
        addCities();
        addSourceDestinationMapping();
        addTravelClasses();
    }

    public void addCities(){
        city.add("Hyderabad");
        city.add("Chennai");
        city.add("Pune");
        city.add("Delhi");
        city.add("Bangalore");
    }

    public void addFlightDetails(){
        flightsList = new ArrayList<>();
        flightsList.add(flight1);
        flightsList.add(flight2);
        flightsList.add(flight3);
    }
    public List<FlightDetails> retrieveFlightsList(){
        return flightsList;
    }

    public void addSourceDestinationMapping(){

        sourceDestinationFlights = new ArrayList<FlightRoute>();
        sourceDestinationFlights.add(route1);
        sourceDestinationFlights.add(route2);
        sourceDestinationFlights.add(route3);
        sourceDestinationFlights.add(route4);
        sourceDestinationFlights.add(route5);
        sourceDestinationFlights.add(route6);
        sourceDestinationFlights.add(route7);
        sourceDestinationFlights.add(route8);
    }
    public List<FlightRoute> retrieveSourceDestinationMapping(){
        //System.out.println(sourceDestinationFlights);
        return sourceDestinationFlights;
    }



    public List<String> retrieveCities(){
       // System.out.println(city);
        return city;
    }
    public List<String> retrieveFlights(){
        return flight;
    }



    public void addTravelClasses(){

        TravelClass class1 = new TravelClass(1001, 1,"economy",6000,195);
        TravelClass class2 = new TravelClass(1001, 1,"Business",13000,35);
        TravelClass class3 = new TravelClass(1001, 1,"FirstClass",20000,8);
        TravelClass class4 = new TravelClass(1001, 4,"economy",6000,10);
        TravelClass class5 = new TravelClass(1001, 4,"Business",13000,35);
        TravelClass class6 = new TravelClass(1001, 4,"FirstClass",20000,8);

        TravelClass class7 = new TravelClass(1002, 2,"economy",4000,70);
        TravelClass class8 = new TravelClass(1002, 5,"economy",4000,144);
        TravelClass class9 = new TravelClass(1002, 6,"economy",4000,144);
        TravelClass class10= new TravelClass(1002, 7,"economy",4000,144);

        TravelClass class11 = new TravelClass(1003, 3,"economy",5000,152);
        TravelClass class12 = new TravelClass(1003, 3,"Business",10000,20);
        TravelClass class13 = new TravelClass(1003, 8,"economy",5000,152);
        TravelClass class14 = new TravelClass(1003, 8,"Business",10000,20);



        travelClassList.add(class1);
        travelClassList.add(class2);
        travelClassList.add(class3);
        travelClassList.add(class4);
        travelClassList.add(class5);
        travelClassList.add(class6);
        travelClassList.add(class7);
        travelClassList.add(class8);
        travelClassList.add(class9);
        travelClassList.add(class10);
        travelClassList.add(class11);
        travelClassList.add(class12);
        travelClassList.add(class13);
        travelClassList.add(class14);
    }
    public List<TravelClass> retrieveTravelClasses(){

        return travelClassList;
    }


}
