package org.vapasi.model;

public class TravelClass {
    private int flightID;
    private int routeID;
    private String classOfTravel;
    private double basePrice;
    private int noOfRemainingPassengers;

    public TravelClass(int flightID,int routeID,String classOfTravel,double basePrice,int noOfRemainingPassengers){
        this.flightID = flightID;
        this.routeID = routeID;
        this.classOfTravel = classOfTravel;
        this.basePrice = basePrice;
        this.noOfRemainingPassengers = noOfRemainingPassengers;
    }

    public int getNoOfRemainingPassengers() {
        return noOfRemainingPassengers;
    }

    public void setNoOfRemainingPassengers(int noOfRemainingPassengers) {
        this.noOfRemainingPassengers = noOfRemainingPassengers;
    }

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }

    public int getRouteID() {
        return routeID;
    }

    public void setRouteID(int routeID) {
        this.routeID = routeID;
    }

    public String getClassOfTravel() {
        return classOfTravel;
    }

    public void setClassOfTravel(String classOfTravel) {
        this.classOfTravel = classOfTravel;
    }

    public double getBasePrice() {
        return basePrice;
    }

    public void setBasePrice(double basePrice) {
        this.basePrice = basePrice;
    }
}
