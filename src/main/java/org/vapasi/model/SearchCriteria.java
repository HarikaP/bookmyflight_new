package org.vapasi.model;

import javax.validation.constraints.NotBlank;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Supplier;

import static org.vapasi.service.SearchService.addDays;

public class SearchCriteria {
    private int noOfPassengers;
    @NotBlank(message = "source cannot be empty")
    private String source;
    @NotBlank(message = "source cannot be empty")
    private String destination;
    private String classOfTravel;
    private Date departureDate;
    //private Optional<Date> departureDate;

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNoOfPassengers() {
        return noOfPassengers;
    }

    public void setNoOfPassengers(int noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public String getClassOfTravel() {
        return classOfTravel;
    }

    public void setClassOfTravel(String classOfTravel) {
        this.classOfTravel = classOfTravel;
    }





    public SearchCriteria(int noOfPassengers, @NotBlank(message = "source cannot be empty") String source, @NotBlank(message = "source cannot be empty") String destination, Optional<String> classOfTravel, Optional<Date> departureDate) {

        this.noOfPassengers = noOfPassengers;
        if(noOfPassengers == 0) {
            this.noOfPassengers = 1;
        }
        this.source = source;
        this.destination = destination;

        this.classOfTravel = classOfTravel.orElse("economy");
        //Objects.nonNull(classOfTravel="economy");

        Date dateobj = new Date();
        Date nextDay = addDays(dateobj,1);
        this.departureDate = departureDate.orElse(nextDay);



    }
    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }
    public Date getDepartureDate() {
        return departureDate;
    }
}
