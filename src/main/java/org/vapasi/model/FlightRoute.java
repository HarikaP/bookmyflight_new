package org.vapasi.model;

import org.vapasi.service.SearchService;

import java.util.Date;

public class FlightRoute {
    private int routeID;
    private int flightID;
    private String flightName;

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    private String source;
    private String destination;
    private Date departureDate;
    private String dateOfDeparture;
    //private String classOfTravel;
    private double totalFare;

    public FlightRoute(int routeID, int flightID, String source, String destination, Date departureDate) {
        this.routeID = routeID;
        this.flightID = flightID;
        this.source = source;
        this.destination = destination;
        this.departureDate = departureDate;
        //this.classOfTravel = classOfTravel;
    }

    public double getTotalFare() {
        return totalFare;
    }

    public void setTotalFare(double totalFare) {
        this.totalFare = totalFare;
    }

/*    public String getClassOfTravel() {
        return classOfTravel;
    }

    public void setClassOfTravel(String classOfTravel) {
        this.classOfTravel = classOfTravel;
    }*/

    public int getRouteID() {
        return routeID;
    }

    public void setRouteID(int routeID) {
        this.routeID = routeID;
    }

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }
    public String getDateOfDeparture(){
        dateOfDeparture =  SearchService.getDateInFormat(departureDate);
        return dateOfDeparture;
    }


}
