package org.vapasi.model;

import java.time.LocalDate;
import java.util.Date;

public class FlightDetails {
    private int flightID;
    private String flightName;
    private int noOfEconomySeats;
    private int noOfBusinessSeats;
    private int noOfFirstClassSeats;

    public FlightDetails(int flightID,String flightName,int noOfEconomySeats,int noOfBusinessSeats,int noOfFirstClassSeats){
        this.flightID = flightID;
        this.flightName = flightName;
        this.noOfBusinessSeats = noOfBusinessSeats;
        this.noOfEconomySeats = noOfEconomySeats;
        this.noOfFirstClassSeats = noOfFirstClassSeats;
    }

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }

    public String getFlightName() {
        return flightName;
    }

    public void setFlightName(String flightName) {
        this.flightName = flightName;
    }

    public int getNoOfEconomySeats() {
        return noOfEconomySeats;
    }

    public void setNoOfEconomySeats(int noOfEconomySeats) {
        this.noOfEconomySeats = noOfEconomySeats;
    }

    public int getNoOfBusinessSeats() {
        return noOfBusinessSeats;
    }

    public void setNoOfBusinessSeats(int noOfBusinessSeats) {
        this.noOfBusinessSeats = noOfBusinessSeats;
    }

    public int getNoOfFirstClassSeats() {
        return noOfFirstClassSeats;
    }

    public void setNoOfFirstClassSeats(int noOfFirstClassSeats) {
        this.noOfFirstClassSeats = noOfFirstClassSeats;
    }


   /* private String classOfTravel;
    private double basePrice;
    private double totalFare;
    private Date departureDate;*/
    /*private String source;
    private String destination;
    private int noOfPassengers;*/







}
