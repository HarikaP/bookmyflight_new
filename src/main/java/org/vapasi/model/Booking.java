package org.vapasi.model;

import java.util.Date;

public class Booking {
    private int flightID;
    private int routeID;
    private int noOfPassengers;
    private int remainingNoOfPassengers;
    private String classOfTravel;
    private Date departureDate;
    private double totalPrice;

    public int getFlightID() {
        return flightID;
    }

    public void setFlightID(int flightID) {
        this.flightID = flightID;
    }

    public int getRouteID() {
        return routeID;
    }

    public void setRouteID(int routeID) {
        this.routeID = routeID;
    }

    public int getNoOfPassengers() {
        return noOfPassengers;
    }

    public void setNoOfPassengers(int noOfPassengers) {
        this.noOfPassengers = noOfPassengers;
    }

    public int getRemainingNoOfPassengers() {
        return remainingNoOfPassengers;
    }

    public void setRemainingNoOfPassengers(int remainingNoOfPassengers) {
        this.remainingNoOfPassengers = remainingNoOfPassengers;
    }

    public String getClassOfTravel() {
        return classOfTravel;
    }

    public void setClassOfTravel(String classOfTravel) {
        this.classOfTravel = classOfTravel;
    }

    public Date getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(Date departureDate) {
        this.departureDate = departureDate;
    }

}
