package org.vapasi.model;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.List;
import java.util.Map;

public class AjaxResponseBody {
    private String msg;
    private List<FlightRoute> result;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public List<FlightRoute> getResult() {
        return result;
    }

    public void setResult(List<FlightRoute> result) {
        this.result = result;
    }

   // ResultTable resultTable = ObjectMapper.readValue(result,ResultTable.class);
}
