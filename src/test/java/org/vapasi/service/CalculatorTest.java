package org.vapasi.service;

import org.junit.Test;
import org.mockito.Mock;
import org.vapasi.model.FlightDetails;
import org.vapasi.model.FlightRoute;
import org.vapasi.model.SearchCriteria;
import org.vapasi.model.TravelClass;
import org.vapasi.repository.FlightRepository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CalculatorTest {
    Map<FlightDetails, String> mockedMap;

    public CalculatorTest() throws ParseException {
    }

    public List<FlightRoute> returnFlightValues() throws ParseException {
        String sDate1 = "31/08/2019";
        String sDate2 = "30/08/2019";
        String sDate3 = "30/08/2019";
        String sDate4 = "03/09/2019";
        String sDate5 = "25/08/2019";
        String sDate6 = "29/08/2019";
        String sDate7 = "25/08/2019";
        String sDate8 = "01/09/2019";

        Date date1 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate1);
        Date date2 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate2);
        Date date3 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate3);
        Date date4 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate4);
        Date date5 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate5);
        Date date6 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate6);
        Date date7 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate7);
        Date date8 = new SimpleDateFormat("dd/MM/yyyy").parse(sDate8);


        FlightRoute route1 = new FlightRoute(1,1001,"Hyderabad","Delhi",date1);
        FlightRoute route2 = new FlightRoute(2,1002,"Chennai","Bangalore",date2);
        FlightRoute route3 = new FlightRoute(3,1003,"Delhi","Bangalore",date3);
        FlightRoute route4 = new FlightRoute(4,1001,"Hyderabad","Pune",date4);
        FlightRoute route5 = new FlightRoute(5,1002,"Chennai","Delhi",date5);
        FlightRoute route6 = new FlightRoute(6,1002,"Delhi","Bangalore",date6);
        FlightRoute route7 = new FlightRoute(7,1002,"Pune","Bangalore",date7);
        FlightRoute route8 = new FlightRoute(8,1003,"Delhi","Chennai",date8);

        List<FlightRoute> routes = new ArrayList<>();
        routes.add(route1);
        routes.add(route2);
        routes.add(route3);
        routes.add(route4);
        routes.add(route5);
        routes.add(route6);
        routes.add(route7);
        routes.add(route8);
        System.out.println(routes);
        return routes;
    }

    public List<TravelClass> retrieveTravelClasses() {

        TravelClass class1 = new TravelClass(1001, 1,"economy",6000,195);
        TravelClass class2 = new TravelClass(1001, 1,"Business",13000,35);
        TravelClass class3 = new TravelClass(1001, 1,"FirstClass",20000,8);
        TravelClass class4 = new TravelClass(1001, 4,"economy",6000,10);
        TravelClass class5 = new TravelClass(1001, 4,"Business",13000,35);
        TravelClass class6 = new TravelClass(1001, 4,"FirstClass",20000,8);

        TravelClass class7 = new TravelClass(1002, 2,"economy",4000,70);
        TravelClass class8 = new TravelClass(1002, 5,"economy",4000,144);
        TravelClass class9 = new TravelClass(1002, 6,"economy",4000,144);
        TravelClass class10= new TravelClass(1002, 7,"economy",4000,144);

        TravelClass class11 = new TravelClass(1003, 3,"economy",5000,152);
        TravelClass class12 = new TravelClass(1003, 3,"Business",10000,20);
        TravelClass class13 = new TravelClass(1003, 8,"economy",5000,152);
        TravelClass class14 = new TravelClass(1003, 8,"Business",10000,20);

        List<TravelClass> travelClassList = new ArrayList<>();
        travelClassList.add(class1);
        travelClassList.add(class2);
        travelClassList.add(class3);
        travelClassList.add(class4);
        travelClassList.add(class5);
        travelClassList.add(class6);
        travelClassList.add(class7);
        travelClassList.add(class8);
        travelClassList.add(class9);
        travelClassList.add(class10);
        travelClassList.add(class11);
        travelClassList.add(class12);
        travelClassList.add(class13);
        travelClassList.add(class14);
        return travelClassList;
    }

    public List<FlightDetails> retrieveFlights() {
        FlightDetails flight1 = new FlightDetails(1001, "Boeing 777-200LR(77L)", 195, 35, 8);
        FlightDetails flight2 = new FlightDetails(1002, "Airbus A319 V2", 144, 0, 0);
        FlightDetails flight3 = new FlightDetails(1003, "Airbus A321", 152, 20, 0);
        List<FlightDetails> flights = new ArrayList<>();
        flights.add(flight1);
        flights.add(flight2);
        flights.add(flight3);
        return flights;
    }
    Calculator calculator = new EconomyBasePriceCalculator();
    Calculator calculator1 = new BusinessBasePriceCalculator();
    Calculator calculator2 = new FirstClassBasePriceCalculator();

    @Test
    public void returnTotalPriceForGivenPassengers() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        //Calculator calculator = new EconomyBasePriceCalculator();
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("31-08-2019");
        List<FlightRoute> routes = searchService.searchFlights("Hyderabad", "Delhi", 1, date, "economy");
        for (FlightRoute x : routes) {
           /* FlightRoute f = new FlightRoute(x.getRouteID(), x.getFlightID(), x.getSource(), x.getDestination(), x.getDepartureDate());
            calculator.calculateTotalFare(1,calculator.calculateBasePriceValue(f,1,""))
            // double basePriceValue = searchService.basePriceCalculationForEconomy(1, 195, f.getEconomyBasePrice());
            //double totalFare = searchService.costOfBooking(1, basePriceValue);*/
            assertEquals(6000, x.getTotalFare(),4);//calculator.calculateTotalFare(1,f,"economy"), 4);
        }

    }

    @Test
    public void returnTotalPriceForGivenPassengersForEconomyForNext50percentSeats() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("30-08-2019");
        List<FlightRoute> routes = searchService.searchFlights("Chennai", "Bangalore", 1, date, "economy");
        for (FlightRoute x : routes) {
           /* FlightRoute f = new FlightRoute(x.getRouteID(), x.getFlightID(), x.getFlightName(), x.getSource(), x.getDestination(), x.getDepartureDate(), x.getEconomyBasePrice(), x.getBusinessBasePrice(), x.getFirstClassBasePrice());
            //double basePriceValue = searchService.basePriceCalculationForEconomy(2, 144, f.getEconomyBasePrice());
            //double totalFare = searchService.costOfBooking(2, basePriceValue);*/
            assertEquals(5200,x.getTotalFare(),4);

        }
    }

    @Test
    public void returnTotalPriceForGivenPassengersForEconomyForFirst40percentSeats() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("30-08-2019");

        List<FlightRoute> actualResult = searchService.searchFlights("Delhi", "Bangalore", 2, date, "economy");
        for (FlightRoute x : actualResult) {
           /* System.out.println(x.getDepartureDate());
            FlightRoute f = new FlightRoute(x.getRouteID(), x.getFlightID(), x.getFlightName(), x.getSource(), x.getDestination(), x.getDepartureDate(), x.getEconomyBasePrice(), x.getBusinessBasePrice(), x.getFirstClassBasePrice());
            //double basePriceValue = searchService.basePriceCalculationForEconomy(2, 144, f.getEconomyBasePrice());
            //double totalFare = searchService.costOfBooking(2, basePriceValue);*/
            assertEquals(10000, x.getTotalFare(),4);

        }
    }

    @Test
    public void returnTotalPriceForGivenPassengersForEconomyForLast10percentSeats() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("30-08-2019");
        List<FlightRoute> routes = searchService.searchFlights("Hyderabad", "Pune", 3, date, "economy");
        for (FlightRoute x : routes) {
            assertEquals(28800,x.getTotalFare(),4);
        }

    }


    @Test
    public void returnTotalPriceForGivenPassengersForBusinessForMoFriOrSun() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("30-08-2019");
        List<FlightRoute> routes = searchService.searchFlights("Delhi", "Bangalore", 2, date, "Business");
        for (FlightRoute x : routes) {
            assertEquals(28000, x.getTotalFare(),4);

        }
    }
    @Test
    public void returnTotalPriceForGivenPassengersForBusinessForTueWedThurSat() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("03-09-2019");
        List<FlightRoute> routes = searchService.searchFlights("Hyderabad", "Pune", 2, date, "Business");
        for (FlightRoute x : routes) {
           // FlightRoute f = new FlightRoute(x.getRouteID(),x.getFlightID(),x.getFlightName() ,x.getSource(), x.getDestination(), x.getDepartureDate(),x.getEconomyBasePrice(),x.getBusinessBasePrice(),x.getFirstClassBasePrice());
            // double basePriceValue = searchService.basePriceCalculatorForBusiness(x.getDepartureDate(),x.getBusinessBasePrice());
            //double totalFare = searchService.costOfBooking(2,basePriceValue);
            assertEquals(26000,  x.getTotalFare(),4);
        }
    }
    @Test
    public void returnNullIfBookingForFirstClassIsDoneMoreThanBefore10DaysOfDeparture() throws ParseException {
        FlightRepository flightRepository = mock(FlightRepository.class);
        when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
        when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
        SearchService searchService = new SearchService();
        searchService.setRep(flightRepository);
        Date date = new SimpleDateFormat("dd-MM-yyyy").parse("04-09-2019");
        List<FlightRoute> routes = searchService.searchFlights("Hyderabad", "Delhi", 2, date, "FirstClass");
        assertEquals(0, routes.size(), 4);
    }

        @Test
        public void returnTotalFareForFirstClassIfBookingIsDoneUptoTo10DaysBeforeDateOfDeparture() throws ParseException {
            FlightRepository flightRepository = mock(FlightRepository.class);
            when(flightRepository.retrieveSourceDestinationMapping()).thenReturn(returnFlightValues());
            when(flightRepository.retrieveTravelClasses()).thenReturn(retrieveTravelClasses());
            SearchService searchService = new SearchService();
            searchService.setRep(flightRepository);
            Date date = new SimpleDateFormat("dd-MM-yyyy").parse("31-08-2019");
            List<FlightRoute> routes = searchService.searchFlights("Hyderabad", "Delhi", 2, date, "FirstClass");
            for (FlightRoute x : routes) {
                assertEquals(76000,x.getTotalFare(),4);
            }
        }

}