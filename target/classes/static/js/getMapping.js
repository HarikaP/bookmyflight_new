$(document).ready(function () {

    $("#search").click(function (event) {
        console.log("---->",event);

        //stop submit the form, we will post it manually.
        event.preventDefault();

        fire_ajax_submit();

    });

});

function fire_ajax_submit() {

    let input = {}
    input["source"] = $("#source").val();
    input["destination"] = $("#destination").val();
    input["noOfPassengers"] = $("#noOfPassengers").val() ;
    input["departureDate"] = $("#departureDate").val();
    input["classOfTravel"] =  $("[name='classOfTravel']:checked").val();

    $("#search").prop("disabled", true);

    $.ajax({
        type: "POST",
        contentType: "application/json",
        url: "/flights/search",
        data: JSON.stringify(input),
        dataType: 'json',
        cache: false,
        timeout: 600000,
        success: function (data) {

         var json = JSON.stringify(input, null, 4) + input+"</pre>";
            $('#list').html(json);


        /*    console.log("SUCCESS : ", data);
            $("#search").prop("disabled", false);

        },*/
        //function createTableFromJSON() {

        console.log("SUCCESS:",data);
            let flightsData = data.result;
            //let flightsData = json.result;
            let tableHeaders = ["source","destination","dateOfDeparture","flightID","totalFare"];

        let table = document.createElement("table");
        let tr = table.insertRow(-1);
        for (let i = 0; i < tableHeaders.length; i++) {
            let th = document.createElement("th");
            th.innerHTML = tableHeaders[i];
            tr.appendChild(th);
        }
        for (let i = 0; i < flightsData.length; i++) {
            tr = table.insertRow(-1);
            for (let j = 0; j < tableHeaders.length; j++) {
                let tabCell = tr.insertCell(-1);
                tabCell.innerHTML = flightsData[i][tableHeaders[j]];
            }
        }
        let divContainer = document.getElementById("list");
        divContainer.innerHTML = "";
        divContainer.appendChild(table);
       /* divContainer = document.getElementById("noOfPassengers");
        divContainer.innerHTML += "";
        divContainer.appendChild(table);*/
       //${list()}
            $("#search").prop("disabled", false);
    },

        error: function (e) {

            let json = "<h4>Ajax Response</h4><pre>"
                + e.responseText + "</pre>";
            $('#list').html(json);

            console.log("ERROR : ", e);
            $("#search").prop("disabled", false);

        }
    });


}